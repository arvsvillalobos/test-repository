-- Create the "inet" database
CREATE DATABASE inet;

-- Switch to the newly created database
USE inet;

-- Create the "patients" table
CREATE TABLE patients (
    patientId INT PRIMARY KEY AUTO_INCREMENT, -- Auto-incrementing ID for simplicity
    firstname VARCHAR(50) NOT NULL,  -- Typical max length for names
    lastname VARCHAR(50) NOT NULL,
    middlename VARCHAR(50),
    birthdate DATE NOT NULL,
    gender CHAR(1), -- 'M' for male, 'F' for female or '0' for others
    civil_status VARCHAR(20), -- Allows for various statuses
    address VARCHAR(255), -- Generous length for most addresses
    email_address VARCHAR(100), -- Can accommodate longer email addresses
    contact_number VARCHAR(20) -- Phone numbers can include symbols such as +, -, etc.
);

-- Create the "examinations" table
CREATE TABLE examinations (
    examId INT PRIMARY KEY AUTO_INCREMENT,
    patientId INT NOT NULL, 
    exam_name VARCHAR(50) NOT NULL,
    results TEXT DEFAULT 'pending', -- Allows for longer text like paragraphs if need be
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- Timestamp of record creation 

    -- Define foreign key relationship
    CONSTRAINT FK_examinations_patients FOREIGN KEY (patientId) REFERENCES patients(patientId)
);

-- Create unique index so no duplicate entries for the same exact patient
CREATE UNIQUE INDEX idx_patient_name ON patients (firstname, lastname, middlename); 
