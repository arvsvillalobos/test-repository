-- SQL QUERIES 

-- 1. Write a SQL statement that adds a new record on the patient table.

INSERT INTO patients (firstname, lastname, middlename, birthdate, gender, civil_status, address, email_address, contact_number)
VALUES ('Haffaz', 'Aladeen', 'Muadib', '1985-05-12', 'M', 'Single', '911 Wadiya Street', 'admiral.general@wadiya.com', '+63 976-555-1212');

-- 2. Write a SQL statement that updates 1 record on the patient table.

UPDATE patients 
SET email_address = 'allison.burgers@email.com', contact_number = '+1 555-9876' 
WHERE patientId = 0; -- Unique identifier so only 1 will match this

-- 3. Write a SQL statement that removes 1 record on the patient table.

DELETE FROM patients 
WHERE patientId = 0; -- Unique identifier so only 1 will match this

-- 4. Write a SQL statement that displays all the records on the patient table and arrange it by age.

SELECT * 
FROM patients 
ORDER BY birthdate; -- No need to put ASC as it is default 

-- 5. Write a SQL statement that displays columns from Patients and Examinations tables.

-- This will return duplicates for patientId as both tables have them.
SELECT COLUMN_NAME -- TABLE_NAME and DATA_TYPE is optional in case you want more context
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME IN ('table1', 'table2')
ORDER BY TABLE_NAME -- Optional but deemed necessary for organization

-- Another way to tackle this problem:
-- This also outputs patientId duplicates
SELECT * 
FROM patients
JOIN examinations ON patients.patientId = examinations.patientId;

-- 6. Write a SQL statement that counts all the records on the patient table.

SELECT COUNT(*) AS number_of_patients -- Aliasing to give context 
FROM patients;
