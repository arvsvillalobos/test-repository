n = str(input("Give me an integer(no decimals): "))


def solve(n):
    sum = int(n) + int(n * 2) + int(n * 3)

    return sum


print(f"{n} + {n*2} + {n*3} = {solve(n)}")
