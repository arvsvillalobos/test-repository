sample_list = ["z", "i", "n", "o", "g", "r", "e"]


def list_to_string(list):
    string = ""
    for char in list:
        string += char
    return string


string = list_to_string(sample_list)
string = " + ".join(string)

print(f"String: {string}")
