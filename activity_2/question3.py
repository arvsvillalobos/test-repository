string = input("I will count the characters in the string you will provide: ")


def count(string):
    count = len(string)
    return count


print(f'The string "{string}" has {count(string)} characters')
