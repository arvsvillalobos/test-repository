input_num = input("Give me a set of numbers separated by commas: ")
num_list = input_num.split(",")
# print(type(num_list))


def list_to_tuple(num_list):
    num_tuple = tuple(num_list)
    return num_tuple


num_tuple = list_to_tuple(num_list)

print(f"List: {num_list}")
# print(type(num_list))
print(f"Tuple: {num_tuple}")
# print(type(num_tuple))
