from django.contrib.auth import views as auth_views
from django.urls import path
from django.views.generic.base import RedirectView
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = [
    path("register/", views.register, name="register"),
    path("dashboard/", views.employee_dashboard, name="employee_dashboard"),
    path(
        "",
        RedirectView.as_view(url="supervisor/dashboard/", permanent=False),
        name="home",
    ),
    path(
        "login/",
        views.login_view,
        name="login",
    ),
    path("logout/", views.custom_logout, name="logout"),
    path(
        "supervisor/dashboard", views.supervisor_dashboard, name="supervisor_dashboard"
    ),
    path(
        "supervisor/add_employee/",
        views.add_employee,
        name="add_employee",
    ),
    path(
        "supervisor/add_transaction/",
        views.add_transaction,
        name="add_transaction",
    ),
    path(
        "supervisor/view_employee/<int:employee_id>",
        views.view_employee,
        name="view_employee",
    ),
    path(
        "supervisor/view_transaction/<int:transaction_id>",
        views.view_transaction,
        name="view_transaction",
    ),
    path("supervisor/transactions/", views.transactions, name="transactions"),
    path(
        "supervisor/edit_employee/<int:employee_id>",
        views.edit_employee,
        name="edit_employee",
    ),
    path(
        "supervisor/edit_transaction/<int:transaction_id>",
        views.edit_transaction,
        name="edit_transaction",
    ),
]
urlpatterns.insert(
    0,
    path(
        "",
        login_required(
            RedirectView.as_view(url="supervisor/dashboard/", permanent=False)
        ),
        name="login_or_dashboard_redirect",
    ),
)
