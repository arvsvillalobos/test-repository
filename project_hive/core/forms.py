from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Employee, Transaction


class EmployeeForm(forms.ModelForm):
    supervisor = forms.ModelChoiceField(
        queryset=Employee.objects.filter(is_supervisor=True),
        label="Supervisor",
        widget=forms.Select(attrs={"class": "form-control"}),
    )

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["supervisor"].widget.attrs["readonly"] = True
        self.fields["supervisor"].initial = self.get_current_user_employee(user)

    def get_current_user_employee(self, user):
        try:
            employee = Employee.objects.get(user=user)
            return employee
        except Employee.DoesNotExist:
            return None

    class Meta:
        model = Employee
        fields = [
            "first_name",
            "last_name",
            "email",
            "department",
            "supervisor",
            "position",
            "birthdate",
            "sex",
            "hiring_date",
            "status",
        ]
        widgets = {
            "first_name": forms.TextInput(attrs={"class": "form-control"}),
            "last_name": forms.TextInput(attrs={"class": "form-control"}),
            "email": forms.EmailInput(attrs={"class": "form-control"}),
            "department": forms.Select(attrs={"class": "form-control"}),
            "position": forms.TextInput(attrs={"class": "form-control"}),
            "sex": forms.Select(attrs={"class": "form-control"}),
            "status": forms.Select(attrs={"class": "form-control"}),
            "birthdate": forms.DateInput(
                attrs={"class": "form-control", "type": "date"}
            ),
            "hiring_date": forms.DateInput(
                attrs={"class": "form-control", "type": "date"}
            ),
        }

    def label_from_instance(self, obj):
        return (
            obj.get_full_name()
        )  # Assuming you have a method to get the full name of an employee


class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ["employee", "category", "status", "notes", "effective_date"]
        widgets = {
            "employee": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Employee ID# only"}
            ),
            "category": forms.Select(attrs={"class": "form-control"}),
            "status": forms.Select(attrs={"class": "form-control"}),
            "notes": forms.Textarea(attrs={"class": "form-control", "rows": 3}),
            "effective_date": forms.DateInput(
                attrs={"class": "form-control", "type": "date"}
            ),
        }


class CustomUserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "password1",
            "password2",
        )
