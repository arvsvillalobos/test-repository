from django.contrib import admin
from .models import (
    Employee,
    Transaction,
)

admin.site.register(Employee)
admin.site.register(Transaction)
