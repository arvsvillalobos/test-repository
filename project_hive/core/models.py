from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Employee(models.Model):
    first_name = models.CharField(max_length=55)
    middle_name = models.CharField(max_length=55, blank=True, help_text="Optional")
    last_name = models.CharField(max_length=55)
    email = models.EmailField()
    birthdate = models.DateField()

    department = models.CharField(
        max_length=20,
        choices=(
            ("HR", "HR"),
            ("MIS", "MIS"),
            # ... more choices if needed
        ),
    )
    is_supervisor = models.BooleanField(default=False)

    position = models.CharField(max_length=75)
    supervisor = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        limit_choices_to={"is_supervisor": True},  # Limit choices to supervisors only
    )

    sex = models.CharField(
        max_length=1, choices=(("M", "Male"), ("F", "Female"))
    )  # Or expand choices
    hiring_date = models.DateField(default=timezone.now)
    status = models.CharField(
        max_length=20,
        choices=(
            ("Active", "Active"),
            ("Inactive", "Inactive"),
            # ... more choices if needed
        ),
    )
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f"{self.department}#{self.id} - {self.first_name} {self.last_name}"


class Transaction(models.Model):
    TRANSACTION_CATEGORIES = (
        ("Promotion", "Promotion"),
        ("Incident report", "Incident Report"),
        ("Training", "Training"),
        ("Leave", "Leave"),
        ("Absence", "Absence"),
        # Add more categories as needed
    )

    STATUS_CHOICES = (
        ("Open", "Open"),
        ("Closed", "Closed"),
        ("Approved", "Approved"),
        ("Dismissed", "Dismissed"),
        ("Resolved", "Resolved"),
        # Add more status options as needed
    )

    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    category = models.CharField(max_length=50, choices=TRANSACTION_CATEGORIES)
    date_created = models.DateField(auto_now_add=True)  # Auto-set on creation
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    notes = models.TextField(blank=True)
    effective_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"ID#{self.id} {self.category} - {self.employee.last_name}, {self.employee.first_name} ({self.date_created})"
