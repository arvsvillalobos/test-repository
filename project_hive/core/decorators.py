from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect


def group_required(group_name):
    def decorator(view_func):
        def wrapper(request, *args, **kwargs):
            if request.user.groups.filter(name=group_name).exists():
                return view_func(request, *args, **kwargs)
            else:
                return redirect(
                    "login"
                )  # Redirect to login page if user is not in the group

        return wrapper

    return decorator
