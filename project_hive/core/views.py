from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User
from django.contrib import messages
from django.http import JsonResponse, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import MultipleObjectsReturned
from .forms import EmployeeForm, TransactionForm, CustomUserCreationForm
from .models import Employee, Transaction
from .decorators import group_required


def login_view(request):
    if request.user.is_authenticated:
        if request.user.groups.filter(name="sup").exists():
            return redirect(
                "supervisor_dashboard"
            )  # Redirect to supervisor dashboard if supervisor
        else:
            return redirect(
                "employee_dashboard"
            )  # Redirect to employee dashboard if not supervisor
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return (
                redirect("supervisor_dashboard")
                if user.groups.filter(name="sup").exists()
                else redirect("employee_dashboard")
            )
        else:
            messages.error(request, "Invalid username or password.")
    return render(request, "login.html")


def custom_logout(request):
    logout(request)
    return redirect("login")  # Redirect to the login page


def signup_view(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST)
        if form.is_valid():
            user = form.save()  # Save the User object
            # Match and Create Employee object
            employee = Employee.objects.filter(
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
            ).first()

            if employee:
                employee.user = user
                employee.save()
                login(request, user)  # Log in the user
                return redirect("some-success-page")
            else:
                # Handle employee not found case
                pass
    else:
        form = EmployeeForm()
    return render(request, "signup.html", {"form": form})


@login_required
@group_required("sup")
def supervisor_dashboard(request):
    current_user_employee_id = None
    try:
        employee = Employee.objects.get(user=request.user)
        current_user_employee_id = employee.id
    except Employee.DoesNotExist:
        pass

    employees = Employee.objects.filter(
        supervisor=current_user_employee_id, status="active"
    )
    transactions = Transaction.objects.filter(status="open")

    # pagination
    emp_page = request.GET.get("emp_page")
    paginator = Paginator(employees, 10)  # Show 10 employees per page
    try:
        employees = paginator.page(emp_page)
    except PageNotAnInteger:
        employees = paginator.page(1)
    except EmptyPage:
        employees = paginator.page(paginator.num_pages)

    trans_page = request.GET.get("trans_page")
    paginator = Paginator(transactions, 10)  # Show 10 transactions per page
    try:
        transactions = paginator.page(trans_page)
    except PageNotAnInteger:
        transactions = paginator.page(1)
    except EmptyPage:
        transactions = paginator.page(paginator.num_pages)

    return render(
        request,
        "supervisor/dashboard.html",
        {"employees": employees, "transactions": transactions},
    )


@login_required
@group_required("sup")
def add_employee(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect(
                "supervisor_dashboard"
            )  # Redirect to the employee dashboard after adding employee
    else:
        form = EmployeeForm(user=request.user)
    return render(request, "supervisor/add_employee.html", {"form": form})


@login_required
@group_required("sup")
def edit_employee(request, employee_id):
    employee = get_object_or_404(Employee, pk=employee_id)
    if request.method == "POST":
        form = EmployeeForm(request.POST, instance=employee, user=request.user)
        if form.is_valid():
            form.save()
            return redirect("view_employee", employee_id=employee_id)
    else:
        form = EmployeeForm(instance=employee, user=request.user)
    return render(
        request, "supervisor/edit_employee.html", {"form": form, "employee": employee}
    )


@login_required
@group_required("sup")
def add_transaction(request):
    if request.method == "POST":
        form = TransactionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("supervisor_dashboard")
    else:
        form = TransactionForm()
    return render(request, "supervisor/add_transaction.html", {"form": form})


@login_required
def employee_dashboard(request):
    # Get the logged-in user
    user = request.user

    # Assuming each user corresponds to one employee
    employee = Employee.objects.get(user=user)

    # Retrieve transactions associated with the employee
    transactions = Transaction.objects.filter(employee=employee)

    return render(
        request,
        "employee/dashboard.html",
        {"employee": employee, "transactions": transactions, "user": user},
    )


@login_required
def view_employee(request, employee_id):
    try:
        employee = Employee.objects.get(pk=employee_id)
        transactions = Transaction.objects.filter(employee=employee)
    except Employee.DoesNotExist:
        employee = None
        transactions = None

    return render(
        request,
        "supervisor/view_employee.html",
        {"employee": employee, "transactions": transactions},
    )


@login_required
def view_transaction(request, transaction_id):
    try:
        transaction = Transaction.objects.get(pk=transaction_id)
        employee = Employee.objects.get(id=transaction.employee.id)
    except Transaction.DoesNotExist:
        transaction = None
        employee = None

    return render(
        request,
        "supervisor/view_transaction.html",
        {"employee": employee, "transaction": transaction},
    )


@login_required
@group_required("sup")
def edit_transaction(request, transaction_id):
    transaction = get_object_or_404(Transaction, pk=transaction_id)
    if request.method == "POST":
        form = TransactionForm(request.POST, instance=transaction)
        if form.is_valid():
            form.save()
            return redirect("view_transaction", transaction_id=transaction_id)
    else:
        form = TransactionForm(instance=transaction)
    return (
        render(
            request,
            "supervisor/edit_transaction.html",
            {"form": form, "transaction": transaction},
        )
        @ login_required
    )


@login_required
@group_required("sup")
def transactions(request):
    transactions = Transaction.objects.all()

    # Filtering by category
    category_filter = request.GET.get("category")
    if category_filter:
        transactions = transactions.filter(category=category_filter)

    # Filtering by status
    status_filter = request.GET.get("status")
    if status_filter:
        transactions = transactions.filter(status=status_filter)

    # Sorting
    sort_by = request.GET.get("sort_by")
    if sort_by == "date_created_asc":
        transactions = transactions.order_by("date_created")
    elif sort_by == "date_created_desc":
        transactions = transactions.order_by("-date_created")
    elif sort_by == "effective_date_asc":
        transactions = transactions.order_by("effective_date")
    elif sort_by == "effective_date_desc":
        transactions = transactions.order_by("-effective_date")

    paginator = Paginator(transactions, 10)  # Show 10 transactions per page
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    return render(
        request,
        "supervisor/transactions.html",
        {"transactions": page_obj, "paginator": paginator},
    )


def register(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get("first_name")
            last_name = form.cleaned_data.get("last_name")
            email = form.cleaned_data.get("email")
            username = form.cleaned_data.get("username")

            # Check if the employee exists
            employee_exists = Employee.objects.filter(
                first_name=first_name, last_name=last_name, email=email
            ).exists()

            if not employee_exists:
                messages.error(request, "No employee record found")
                return redirect("register")

            # Check if the user already exists
            user_exists1 = User.objects.filter(email=email).exists()
            user_exists2 = User.objects.filter(username=username).exists()

            if user_exists1 or user_exists2:
                messages.error(request, "User already exists")
                return redirect("register")

            # Proceed with registration
            user = form.save()
            group = Group.objects.get(name="emp")  # Assuming 'emp' is the group name
            user.groups.add(group)

            # Update the Employee instance with user.id
            employee = Employee.objects.get(
                first_name=first_name, last_name=last_name, email=email
            )
            employee.user_id = user.id
            employee.save()

            messages.success(request, "Registration successful")
            return redirect(
                "login"
            )  # Redirect to the login page after successful registration
        else:
            messages.error(request, "Form is not valid")
    else:
        form = CustomUserCreationForm()

    return render(request, "register.html", {"form": form})
