# Get user input
num = int(input("Give me a non-decimal number: "))


def is_odd(num):
    # Using modulo we can check if there is a remainder
    return num % 2 == 1  # This returns a boolean


if is_odd(num):
    # If True
    print(f"{num} is odd")
else:
    # else or False
    print(f"{num} is even")
