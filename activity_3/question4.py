amt = 10000  # Principal amount
ir = 3.5  # Interest rate in (%)
years = 7  # Year(s) of interest

# Optional user input
# amt = float(input("Principal: "))
# ir = float(input("Annual interes (%): "))
# years = int(input("Years: "))


ir = ir / 100  # Converts interest to actual matemathical value (e.g. 3.5% is 0.035)


def money_after(amt, ir, years):
    for year in range(years):
        amt *= 1 + ir
        amt = round(amt, 2)
        # print(amt) printed only for checking
    return amt


# Print the amount after running the function
print(money_after(amt, ir, years))
