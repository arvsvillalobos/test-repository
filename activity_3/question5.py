# Sample data
string = "restart"

# Optional user input
# string = input("Give me a string: ")


def replace_all_but_first(string):
    # Make string lowercase to avoid case sensitivity issues
    string = string.lower()

    # Get first character as this will not be changed
    first = string[0]

    # Remove first character from string
    substr1 = string[1:]

    # Combine the first letter and the changed substring
    string = first + substr1.replace(first, "$")
    return string


# Print the base string
print(string)
# Print the converted string
print(replace_all_but_first(string))
