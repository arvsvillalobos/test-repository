# Code from question number 7
class Pet:
    def __init__(self, dog_name, dog_breed, dog_color):
        self.dog_name = dog_name
        self.dog_breed = dog_breed
        self.dog_color = dog_color


class BigPet(Pet):
    def dog_name_upper(self):  # Function that overrides dog_name
        return self.dog_name.upper()  # Return the uppercased dog name


# Changed pet to BigPet from question number 7
pet = BigPet("Max", "labrador", "black")


# Print the test data
print(
    f"I bought a {pet.dog_color} {pet.dog_breed} and I named my dog {pet.dog_name_upper()}."
)
