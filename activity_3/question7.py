class Pet:
    def __init__(self, dog_name, dog_breed, dog_color):
        self.dog_name = dog_name
        self.dog_breed = dog_breed
        self.dog_color = dog_color


# Create the pet
pet = Pet("Max", "labrador", "black")

# Print the test data
print(f"I bought a {pet.dog_color} {pet.dog_breed} and I named my dog {pet.dog_name}.")
