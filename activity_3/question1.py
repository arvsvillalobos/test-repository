from datetime import datetime as dt

# Get user inputs (optional)
# date1_str = input("Date #1 (YYYY, M, D): ")
# date2_str = input("Date #2 (YYYY, M, D): ")

# Test data
date1_str = "2014, 7, 2"
date2_str = "2014, 7, 11"


def number_days_between(date1, date2):
    # Convert strings to datetime
    date_format = "%Y, %m, %d"
    date1 = dt.strptime(date1_str, date_format)
    date2 = dt.strptime(date2_str, date_format)

    # Calculate the difference
    diff = date2 - date1

    # Return the difference in days
    return abs(diff.days)


print(
    f"The given dates have {number_days_between(date1_str, date2_str)} days in between."
)
