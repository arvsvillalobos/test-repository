def is_within_100_of_1000_or_2000(num):
    # Check if number is within 100 of 1000 or 2000 using absolute values
    return abs(num - 1000) <= 100 or abs(num - 2000) <= 100  # This returns a boolean


test_data = [1000, 900, 800, 2200]

# The test data is a list, so we can use a for loop to iterate over
for num in test_data:
    print(f"{num} = {is_within_100_of_1000_or_2000(num)}")
