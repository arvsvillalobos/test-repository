class Human:
    def __init__(self, name, group):
        self.name = name
        self.group = group


class TitanShifter(Human):
    def __init__(self, name, group, titan_power):
        super().__init__(name, group)  # Uses parent class constructor instances
        self.titan_power = titan_power  # New exclusive constructor

    def self_harm(self):
        print(f"{self.name} transforms into the {self.titan_power}!")


# Sample entry for human class
mikasa = Human("Mikasa Ackerman", "Scouting Legion")

# Sample entry for titan shifter class
eren = TitanShifter("Eren Jaeger", "Scouting Legion", "Attack Titan")

print(f"{mikasa.name} is a member of the {mikasa.group}.")
print(
    f"{eren.name} is a member of the {eren.group}. He also holds the power of the {eren.titan_power}."
)

# Will not work as Mikasa is not a Titan Shifter
# mikasa.self_harm()

# Will work as Eren is indeed a Titan Shifter
eren.self_harm()
