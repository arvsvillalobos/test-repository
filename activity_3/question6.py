from datetime import datetime, timedelta


def combine(val1, val2):
    if isinstance(val1, str) and isinstance(val2, str):  # Checks if both are strings
        return val1 + val2
    elif isinstance(val1, int) and isinstance(val2, int):  # Checks if both are ints
        return val1 * val2
    elif isinstance(val1, list) and isinstance(val2, list):  # Checks if both are lists
        return val1 + val2
    elif isinstance(val1, dict) and isinstance(
        val2, dict
    ):  # Checks if both are dictionaries
        return {**val1, **val2}
    elif isinstance(val1, datetime) and isinstance(
        val2, datetime
    ):  # Checks if both are datetimes
        difference = val1 - val2
        return abs(difference.days)  # Get difference in days in absolute values
    else:
        # Data types are not compatible
        return "NOT COMPATIBLE"


# Sample runs

# Both strings
result = combine("Mi casa ", "es su casa")
print(result)

# Both ints
result = combine(5, 3)
print(result)

# Both lists
result = combine([1, 2, 3], [4, 5, 6])
print(result)

# Both dictionaries
result = combine({"a": 1}, {"b": 2})
print(result)

# Both datetime objects
date1 = datetime.now()
date2 = datetime(2022, 8, 28)
result = combine(date1, date2)
print(result)

# Mismatched data types
result = combine({"a": 1}, date1)
print(result)
